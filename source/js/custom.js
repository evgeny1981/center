jQuery(document).ready(function($) {

    // datepicker localize

    //       $.datepicker.regional['ru'] = {
    //                 closeText: 'Закрыть',
    //                 prevText: '&#x3c;Пред',
    //                 nextText: 'След&#x3e;',
    //                 currentText: 'Сегодня',
    //                 monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
    //                 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    //                 monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
    //                 'Июл','Авг','Сен','Окт','Ноя','Дек'],
    //                 dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
    //                 dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
    //                 dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    //                 weekHeader: 'Нед',
    //                 dateFormat: 'dd.mm.yy',
    //                 firstDay: 1,
    //                 isRTL: false,
    //                 showMonthAfterYear: false,
    //                 yearSuffix: ''};
    //     $.datepicker.setDefaults($.datepicker.regional['ru']);

    // checkout default event links

    $(".link-inactive").click(function(e) {
        e.preventDefault();
    });

    // wrap first word

    $('.first-word-wrap, .preim-block h3').each(function() {
        this.innerHTML = this.innerHTML.replace(/^(.+?\s)/, '<span>$1</span>');
    });

    // reviews slider

    $('.reviews-slider').owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: true,
        autoHeight: true,
        mouseDrag: false
    });

    // event anonce slider

    $('.events-anonce-list').owlCarousel({
        items: 3,
        margin: 3,
        nav: false,
        dots: false,
        loop: true,
        mouseDrag: false,
        responsive: {
            0: {
                items: 1,
                margin: false
            },
            768: {
                items: 2,
                margin: false
            },
            1180: {
                items: 3
            }
        }
    });

    // last blogs slider

    $('.last-blogs-list').owlCarousel({
        items: 3,
        margin: 31,
        nav: false,
        dots: false,
        loop: true,
        mouseDrag: false,
        responsive: {
            0: {
                items: 1,
                margin: false
            },
            768: {
                items: 2,
                margin: false
            },
            1180: {
                items: 3
            }
        }
    });


    // front about

    $(".front-about__full-button").click(function(e) {
        $('.front-about__text').addClass('active');
    });

    // main menu dropdown

    $('.header-bottom__menu > .menu > li.expanded > .menu').addClass('animated fadeInUp');

    // course programm

    $(".course-content__main-text-full-button").click(function(e) {
        $('.course-content__main-text-inner').addClass('active');
    });

    // course order tabs

    $('.course-content__order-tabs').delegate('.course-content__order-tabs-button:not(.active)', 'click', function() {
        $(this).addClass('active').siblings().removeClass('active').closest('.course-content__order').find('.course-content__order-content-form').eq($(this).index()).addClass('active').siblings('.course-content__order-content-form').removeClass('active');
    });

    //select style

    $(".field-select").chosen({
        disable_search: true,
        width: "100%"
    });


    // $('.field-select').on("chosen:showing_dropdown", function () {
    //       $(this).siblings('.chosen-container').find('.chosen-results').jScrollPane({
    //             autoReinitialise: true
    //       });
    // });

    $('.chosen-drop').addClass('animated fadeIn');

    // datepicker

    $(".node-type-kurs").each(function(e) {
        jQuery.datetimepicker.setLocale('ru');
    });

    $('.course-content__top-date-list').each(function() {
        var periodBox = $(this).find('.date-display-range');
        if (periodBox.length === 1) {
            $(this).closest('.node-type-kurs').addClass('period-course');
        } else if (periodBox.length === 2) {
            $(this).closest('.node-type-kurs').addClass('period-two-course');
        }
    });

    $('.date-display-single').closest('.node-type-kurs').addClass('single-date-course');


    $('.period-course').each(function() {
        var minDate = $('.date-display-start').text();
        var maxDate = $('.date-display-end').text();

        $('.date-field').datetimepicker({
            setLocale: 'ru',
            dayOfWeekStart: 1,
            timepicker: false,
            minDate: minDate,
            formatDate: 'd.m.Y',
            format:'d.m.Y',
            maxDate: maxDate
        });

    });

    $('.period-two-course').each(function() {
        var minDate1 = $('.date-display-range:first .date-display-start').text();
        var maxDate1 = $('.date-display-range:first .date-display-end').text();

        var minDate2 = $('.date-display-range:last .date-display-start').text();
        var maxDate2 = $('.date-display-range:last .date-display-end').text();

        console.log(minDate1, maxDate1, minDate2, maxDate2);

        $('.date-field').datetimepicker({
            setLocale: 'ru',
            dayOfWeekStart: 1,
            timepicker: false,
            allowDates: [minDate1, maxDate1, minDate2, maxDate2],
            formatDate: 'd.m.y',
            format:'d.m.Y'
        });

    });

    $('.single-date-course').each(function() {
        var singleDate = $('.date-display-single').text();

        $('.date-field').datetimepicker({
            setLocale: 'ru',
            minDate: singleDate,
            dayOfWeekStart: 1,
            formatDate: 'd.m.Y',
            format:'d.m.Y',
            maxDate: singleDate,
            timepicker: false
        });

    });



    $('.xdsoft_datetimepicker').addClass('animated fadeIn');

    // $('.date-field').datepicker({

    // });

    // serts slider

    $('.coach-content__serts-list').each(function() {
        if ($(this).find('> a').length > 1) {
            $(this).addClass('owl-carousel');
            $(this).owlCarousel({
                margin: 25,
                items: 3,
                loop: true,
                nav: true,
                dots: false,
                mouseDrag: false,
                onChange: callbackGallery,
                responsive: {
                    0: {
                        items: 1,
                        margin: false
                    },
                    768: {
                        items: 2,
                        margin: false
                    },
                    1180: {
                        items: 3
                    }
                }
            });
        }
    });

    function callbackGallery(event) {
        $('.coach-content__serts-list a').fancybox();
    }

    // blog subscribe

    $(".blog-subscribe").each(function(e) {
        var form = $(this);
        $(this).next('.blogs-list').find('.item-2').after(form);
    });

    // clients list

    $('.clients-list img').wrap('<span class="clients-list__logo box-col align-middle"></span>');

    // medium menu

    $('.medium-menu-panel__menu > .menu > li > .menu').addClass('animated fadeInRight');

    $(".burger-button").click(function(e) {
        $(this).toggleClass('active');
        $('.medium-menu-panel').toggleClass('active');
        $('body').toggleClass('no-scroll');
    });

    $(".medium-menu-panel__menu > .menu > .expanded > a").click(function(e) {
        $(this).closest('.expanded').toggleClass('active');
    });

    // button up

    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.back-top').addClass('active');
        } else {
            $('.back-top').removeClass('active');
        }
    });

    $('.back-top').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });







































    //ajax functions

    $(document).ajaxComplete(function() {

        //select style

        $(".field-select").chosen({
            disable_search: true,
            width: "100%"
        });

        $('.field-select').on("chosen:showing_dropdown", function() {
            $(this).siblings('.chosen-container').find('.chosen-results').jScrollPane({});
        });

        $('.chosen-drop').addClass('animated fadeIn');

        $('.xdsoft_datetimepicker').addClass('animated fadeIn');

        // modal title

        $('.modal-title').each(function() {
            this.innerHTML = this.innerHTML.replace(/^(.+?\s)/, '<span>$1</span>');
        });

        // modal closed

        $('.backdrop-default').click(function(event) {
            $('.modal-default .popups-close').click();
        });






    });

    //ajax functions end


    // Chosen touch support.
    // if ($('.chosen-container').length > 0) {
    //   $('.chosen-container').on('touchstart', function(e){
    //     e.stopPropagation(); e.preventDefault();
    //     // Trigger the mousedown event.
    //     $(this).trigger('mousedown');
    //   });
    // }









});
