'use strict';

const gulp = require('gulp');
const fileCache = require('gulp-filter-cache');
const spritesmith = require('gulp.spritesmith');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const nib = require('./node_modules/nib');
const browserSync = require('browser-sync');
const jade = require('gulp-jade-drupal');
const gutil = require('gulp-util');
const ftp = require('gulp-ftp');
const ftpVinyl = require( 'vinyl-ftp' );
const cache = require('gulp-cache');
const beml = require('gulp-beml');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const rsync = require('gulp-rsync');


gulp.task('jsUglify', function() {
    gulp.src('./source/js/custom.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js'))
        .pipe(browserSync.reload({stream: true}))
});


gulp.task('imageMin', function() {
    gulp.src('./source/images/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('./img'))
});


gulp.task('styles', function() {
    return gulp.src('./source/styles/style.styl')
    .pipe(stylus({
        use: nib(),
        compress: true
    }))
    .pipe(plumber())
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.reload({stream: true}))
});


gulp.task('pages', function () {
    gulp.src('./source/layout/**/*.pug')
    .pipe(jade({
        pretty: false,
        locals: {
            title: 'OMG THIS IS THE TITLE'
        }
    }))
    .pipe(beml({
        elemPrefix: '__',
        modPrefix: '_',
        modDlmtr: '_'
    }))
    .pipe(gulp.dest('./layout'))
});

gulp.task('sprite', function() {
    var spriteData =
        gulp.src('./source/images/icons/*.png') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.styl',
                cssFormat: 'stylus',
                cssTemplate: 'stylus.template.mustache'
            }));

    spriteData.img.pipe(gulp.dest('./img/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('./source/styles/')); // путь, куда сохраняем стили
});



gulp.task('ftpStyles', function () {
    return gulp.src('./css/style.css')
        .pipe(ftp({
            remotePath: '/1aid.com.ua/www/sites/all/themes/center/css',
            host: 'medmar.ftp.tools',
            user: 'medmar_ftp',
            pass: 'mz2eb346'
        }))
        .pipe(gutil.noop())
        .pipe(gulp.dest('/1aid.com.ua/www/sites/all/themes/center/css'));
});

gulp.task('ftpPages', function () {
    return gulp.src('./layout/**/*')
        .pipe(ftp({
            remotePath: '/1aid.com.ua/www/sites/all/themes/center/layout',
            host: 'medmar.ftp.tools',
            user: 'medmar_ftp',
            pass: 'mz2eb346'
        }))
        .pipe(gutil.noop())
        .pipe(gulp.dest('/1aid.com.ua/www/sites/all/themes/center/layout'));
});


gulp.task('ftpLayout', function () {
	var conn = ftpVinyl.create( {
		host: 'medmar.ftp.tools',
		user: 'medmar_ftp',
		password: 'mz2eb346',
		parallel: 1,
        maxConnections: 1
	});
	var globs = './layout/**/*';
	return gulp.src(globs, { base: 'layout/', buffer: false })
		.pipe(conn.differentSize('/1aid.com.ua/www/sites/all/themes/center/layout'))
		.pipe(conn.dest('/1aid.com.ua/www/sites/all/themes/center/layout'))
        .pipe(notify({
            message: 'Finished deployment.',
            onLast: false
        }))
        .pipe(browserSync.reload({stream: true}));
});

// gulp.task('deploy', ['deploy'],function() {
//     return gulp.src('./layout/**')
//     .pipe(rsync({
//         root: './layout/',
//         hostname: 'spacesite.tmweb.ru',
//         destination: '/center/public_html/sites/all/themes/center/layout/',
//         archive: true,
//         silent: false,
//         compress: true
//     }));
// });

gulp.task('ftpImages', function () {
    return gulp.src('./img/*.*')
        .pipe(ftp({
            remotePath: '/1aid.com.ua/www/sites/all/themes/center/img',
            host: 'medmar.ftp.tools',
            user: 'medmar_ftp',
            pass: 'mz2eb346'
        }))
        .pipe(gutil.noop())
        .pipe(gulp.dest('/1aid.com.ua/www/sites/all/themes/center/img'));
});

gulp.task('ftpJS', function () {
    return gulp.src('./js/custom.js')
        .pipe(ftp({
            remotePath: '/1aid.com.ua/www/sites/all/themes/center/js',
            host: 'medmar.ftp.tools',
            user: 'medmar_ftp',
            pass: 'mz2eb346'
        }))
        .pipe(gutil.noop())
        .pipe(gulp.dest('/1aid.com.ua/www/sites/all/themes/center/js'));
});


gulp.task('browser-sync', function() {
    browserSync({
        proxy: "http://1aid.com.ua",
        notify: false,
        open: false,
        reloadDelay: 500
    });
});


gulp.task('watch', function() {
    gulp.watch(['./source/styles/style.styl', './source/**/*.styl'], ['styles', 'clear'])
    gulp.watch('./source/layout/**/*.pug', ['pages', 'clear'])
    gulp.watch('./source/js/custom.js', ['jsUglify', 'clear'])
    gulp.watch('./css/style.css', ['clear', 'ftpStyles'])
    gulp.watch('./js/custom.js', ['clear', 'ftpJS'])
    gulp.watch('./layout/**/*', ['clear', 'ftpLayout'])
    gulp.watch('./css/style.css').on('change', browserSync.reload)
    gulp.watch('./js/custom.js').on('change', browserSync.reload)
});

gulp.task('clear', function (done) {
  return cache.clearAll(done);
});

gulp.task('deployPages', ['pages', 'ftpPages']);

gulp.task('default', ['styles', 'pages', 'clear', 'ftpStyles', 'ftpJS', 'ftpLayout', 'watch', 'browser-sync']);
