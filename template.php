<?php 
    function center_preprocess_page(&$vars, $hook) {
        if (isset($vars['node'])) {
            $suggest = "page__node__{$vars['node']->type}";
            $vars['theme_hook_suggestions'][] = $suggest;
        }
        if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
         $term = taxonomy_term_load(arg(2));
         $vars['theme_hook_suggestions'][] = 'page__vocabulary__' . $term->vocabulary_machine_name;
       }
    }
    /**
    * Implements hook_views_pre_render()
    */
    function center_views_pre_render(&$view) {
        if ($view->total_rows && $view->query->offset && $view->query->offset >= $view->total_rows) {
            drupal_not_found();
            exit;
        }
    }
?>
